import {company, address} from 'faker';
import {Mappable} from './CustomMap';

class Company implements Mappable {
  name: string;
  catchPhrase: string;
  location: {
    lat: number;
    lng: number;
  };

  constructor() {
    this.name = company.companyName();
    this.catchPhrase = company.catchPhrase();
    this.location = {
      lat: parseFloat(address.latitude()),
      lng: parseFloat(address.longitude()),
    };
  }
  markerConnect(): string {
    return `
      <h2>Company name: ${this.name}</h2>
      <p>Desc: ${this.catchPhrase}</p>
    `;
  }
}

export default Company;
