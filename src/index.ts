import CustomMap from './CustomMap';
import User from './User';
import Company from './Company';

const map = new CustomMap('map');
map.addMarker(new User());
map.addMarker(new Company());
