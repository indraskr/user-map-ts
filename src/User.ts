import {name, address} from 'faker';
import {Mappable} from './CustomMap';

class User implements Mappable {
  name: string;
  location: {
    lat: number;
    lng: number;
  };

  constructor() {
    this.name = name.firstName();
    this.location = {
      lat: parseFloat(address.latitude()),
      lng: parseFloat(address.longitude()),
    };
  }

  markerConnect(): string {
    return `
      <h2>User name: ${this.name}</h2>
    `;
  }
}

export default User;
