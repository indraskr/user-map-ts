# User Map

### Install deps

```bash
yarn
```

### Run local dev server

```bash
yarn dev
```

### Build app

```bash
yarn build
```
